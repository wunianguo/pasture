﻿/*
*	Copyright(c) 2019 lutianming email：641471957@qq.com
*	Pasture is licensed under the Mulan PSL v1.
*	You can use this software according to the terms and conditions of the Mulan PSL v1.
*	You may obtain a copy of Mulan PSL v1 at :
*	http://license.coscl.org.cn/MulanPSL
*	THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
*	IMPLIED, INCLUDING BUT NOT LIMITED TO NON - INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
*	PURPOSE.
*
*	See the Mulan PSL v1 for more details.
*/

#include "Reactor.h"
#include <Ws2tcpip.h>
#include <time.h>
#include <map>

#pragma comment(lib, "Ws2_32.lib")

#define DATA_BUFSIZE 5120
#define READ	0
#define WRITE	1
#define ACCEPT	2
#define CONNECT 3

static inline void* pst_malloc(size_t size)
{
	return GlobalAlloc(GPTR, size);
}

static inline void* pst_realloc(void* ptr, size_t size)
{
	return GlobalReAlloc(ptr, size, GMEM_MOVEABLE);
}

static inline void pst_free(void* ptr)
{
	GlobalFree(ptr);
}

static inline IOCP_SOCKET* NewIOCP_Socket()
{
	return (IOCP_SOCKET*)pst_malloc(sizeof(IOCP_SOCKET));
}

static inline void ReleaseIOCP_Socket(IOCP_SOCKET* IocpSock)
{
#ifdef KCP_SUPPORT
	if (IocpSock->_conn_type == KCP_CONN)
	{
		Kcp_Content* ctx = (Kcp_Content*)IocpSock->_user_data;
		ikcp_release(ctx->kcp);
		free(ctx->buf);
		free(ctx);
	}
#endif
	pst_free(IocpSock);
}

static inline IOCP_BUFF* NewIOCP_Buff()
{
	return (IOCP_BUFF*)pst_malloc(sizeof(IOCP_BUFF));
}

static inline void ReleaseIOCP_Buff(IOCP_BUFF* IocpBuff)
{
	pst_free(IocpBuff);
}

static SOCKET GetListenSock(const char* addr, int port)
{
	SOCKET listenSock = WSASocket(AF_INET, SOCK_STREAM, 0, NULL, 0, WSA_FLAG_OVERLAPPED);

	SOCKADDR_IN serAdd = {0x0};
	inet_pton(AF_INET, addr, &serAdd.sin_addr);
	serAdd.sin_family = AF_INET;
	serAdd.sin_port = htons(port);

	int ret = bind(listenSock, (SOCKADDR*)& serAdd, sizeof(SOCKADDR));
	if (ret != 0)
	{
		return SOCKET_ERROR;
	}
	listen(listenSock, 5);
	if (listenSock == SOCKET_ERROR)
	{
		return SOCKET_ERROR;
	}
	return listenSock;
}

static void PostAcceptClient(IOCP_SOCKET* IocpSock)
{
	BaseFactory* fc = IocpSock->factory;

	IOCP_BUFF* IocpBuff;
	IocpBuff = NewIOCP_Buff();
	if (IocpBuff == NULL)
	{
		return;
	}
	IocpBuff->databuf.buf = (char*)pst_malloc(DATA_BUFSIZE);
	if (IocpBuff->databuf.buf == NULL)
	{
		ReleaseIOCP_Buff(IocpBuff);
		return;
	}
	IocpBuff->databuf.len = DATA_BUFSIZE;
	IocpBuff->type = ACCEPT;
	IocpBuff->hsock = IocpSock;

	IocpBuff->fd = WSASocket(AF_INET, SOCK_STREAM, 0, NULL, 0, WSA_FLAG_OVERLAPPED);
	if (IocpBuff->fd == INVALID_SOCKET)
	{
		ReleaseIOCP_Buff(IocpBuff);
		return;
	}

	/*调用AcceptEx函数，地址长度需要在原有的上面加上16个字节向服务线程投递一个接收连接的的请求*/
	bool rc = fc->reactor->lpfnAcceptEx(fc->Listenfd, IocpBuff->fd,
		IocpBuff->databuf.buf, 0,
		sizeof(SOCKADDR_IN) + 16, sizeof(SOCKADDR_IN) + 16,
		&IocpBuff->databuf.len, &(IocpBuff->overlapped));

	if (false == rc)
	{
		if (WSAGetLastError() != ERROR_IO_PENDING)
		{
			ReleaseIOCP_Buff(IocpBuff);
			return;
		}
	}
	return;
}

static inline void CloseSocket(IOCP_SOCKET* IocpSock)
{
	SOCKET fd = InterlockedExchange(&IocpSock->fd, INVALID_SOCKET);
	if (fd != INVALID_SOCKET && fd != NULL)
	{
		//CancelIo((HANDLE)fd);	//取消等待执行的异步操作
		closesocket(fd);
	}
}

static inline void AutoProtocolFree(BaseProtocol* proto) {
	AutoProtocol* autoproto = (AutoProtocol*)proto;
	autofree func = autoproto->freefunc;
	func(autoproto);
}

static void Close(IOCP_SOCKET* IocpSock, IOCP_BUFF* IocpBuff, int err)
{
	switch (IocpBuff->type)
	{
	case ACCEPT:
		if (IocpBuff->databuf.buf)
			pst_free(IocpBuff->databuf.buf);
		ReleaseIOCP_Buff(IocpBuff);
		PostAcceptClient(IocpSock);
		return;
	case WRITE:
		if (IocpBuff->databuf.buf != NULL)
			pst_free(IocpBuff->databuf.buf);
		ReleaseIOCP_Buff(IocpBuff);
		return;
	default:
		break;
	}
	BaseProtocol* proto = IocpSock->_user;
	int left_count = 99;
	if (IocpSock->fd != INVALID_SOCKET)
	{
		proto->Lock();
		if (IocpSock->fd != INVALID_SOCKET)
		{
			left_count = InterlockedDecrement(&proto->sockCount);
			if (CONNECT == IocpBuff->type)
				proto->ConnectionFailed(IocpSock, err);
			else
				proto->ConnectionClosed(IocpSock, err);
		}
		proto->UnLock();
	}
	CloseSocket(IocpSock);
	if (left_count == 0 && proto != NULL) {
		switch (proto->protoType)
		{
		case SERVER_PROTOCOL:
			IocpSock->factory->DeleteProtocol(proto);
			break;
		case AUTO_PROTOCOL:
			AutoProtocolFree(proto);
			break;
		default:
			break;
		}
	}

	if (IocpSock->recv_buf)
		pst_free(IocpSock->recv_buf);
	ReleaseIOCP_Buff(IocpBuff);
	ReleaseIOCP_Socket(IocpSock);
	//MemoryBarrier();
}

static bool ResetIocp_Buff(IOCP_SOCKET* IocpSock, IOCP_BUFF* IocpBuff)
{
	memset(&IocpBuff->overlapped, 0, sizeof(OVERLAPPED));

	if (IocpSock->recv_buf == NULL)
	{
		if (IocpBuff->databuf.buf != NULL)
		{
			IocpSock->recv_buf = IocpBuff->databuf.buf;
		}
		else
		{
			IocpSock->recv_buf = (char*)pst_malloc(DATA_BUFSIZE);
			if (IocpSock->recv_buf == NULL)
				return false;
			IocpBuff->size = DATA_BUFSIZE;
		}
	}
	IocpBuff->databuf.len = IocpBuff->size - IocpBuff->offset;
	if (IocpBuff->databuf.len == 0)
	{
		IocpBuff->size += DATA_BUFSIZE;
		char* new_ptr = (char*)pst_realloc(IocpSock->recv_buf, IocpBuff->size);
		if (new_ptr == NULL)
			return false;
		IocpSock->recv_buf = new_ptr;
		IocpBuff->databuf.len = IocpBuff->size - IocpBuff->offset;
	}
	IocpBuff->databuf.buf = IocpSock->recv_buf + IocpBuff->offset;
	return true;
}

static inline void PostRecvUDP(IOCP_SOCKET* IocpSock, IOCP_BUFF* IocpBuff, BaseProtocol* proto)
{
	int fromlen = sizeof(struct sockaddr);
	if (SOCKET_ERROR == WSARecvFrom(IocpSock->fd, &IocpBuff->databuf, 1, NULL, &(IocpBuff->flags), (sockaddr*)&IocpSock->peer_addr, &fromlen, &IocpBuff->overlapped, NULL))
	{
		int err = GetLastError();
		if (ERROR_IO_PENDING != err)
		{
			return Close(IocpSock, IocpBuff, err);
		}
	}
}

static inline void PostRecvTCP(IOCP_SOCKET* IocpSock, IOCP_BUFF* IocpBuff, BaseProtocol* proto)
{
	if (SOCKET_ERROR == WSARecv(IocpSock->fd, &IocpBuff->databuf, 1, NULL, &(IocpBuff->flags), &IocpBuff->overlapped, NULL))
	{
		int err = GetLastError();
		if (ERROR_IO_PENDING != err)
		{
			return Close(IocpSock, IocpBuff, err);
		}
	}
}

static void PostRecv(IOCP_SOCKET* IocpSock, IOCP_BUFF* IocpBuff, BaseProtocol* proto)
{
	IocpBuff->type = READ;
	if (ResetIocp_Buff(IocpSock, IocpBuff) == false)
	{
		return Close(IocpSock, IocpBuff, 14);
	}

	if (IocpSock->_conn_type == TCP_CONN)
		return PostRecvTCP(IocpSock, IocpBuff, proto);
	return PostRecvUDP(IocpSock, IocpBuff, proto);
	
}

static void AceeptClient(IOCP_SOCKET* IocpListenSock, IOCP_BUFF* IocpBuff)
{
	BaseFactory* fc = IocpListenSock->factory;
	Reactor* reactor = fc->reactor;
	setsockopt(IocpBuff->fd, SOL_SOCKET, SO_UPDATE_ACCEPT_CONTEXT, (char*) & (IocpListenSock->fd), sizeof(IocpListenSock->fd));

	IOCP_SOCKET* IocpSock = NewIOCP_Socket();
	if (IocpSock == NULL)
	{
		return Close(IocpListenSock, IocpBuff, 14);
	}
	IocpSock->fd = IocpBuff->fd;
	BaseProtocol* proto = fc->CreateProtocol();	//用户指针
	if (proto == NULL)
	{
		ReleaseIOCP_Socket(IocpSock);
		return Close(IocpListenSock, IocpBuff, 14);
	}
	if (proto->factory == NULL)
		proto->SetFactory(fc, SERVER_PROTOCOL);
	IocpSock->factory = fc;
	IocpSock->_user = proto;	//用户指针
	IocpSock->_IocpBuff = IocpBuff;
	IocpBuff->hsock = IocpSock;

	int nSize = sizeof(IocpSock->peer_addr);
	getpeername(IocpSock->fd, (SOCKADDR*)&IocpSock->peer_addr, &nSize);

	InterlockedIncrement(&proto->sockCount);
	CreateIoCompletionPort((HANDLE)IocpSock->fd, reactor->ComPort, (ULONG_PTR)IocpSock, 0);	//将监听到的套接字关联到完成端口

	proto->Lock();
	proto->ConnectionMade(IocpSock);
	proto->UnLock();

	PostRecv(IocpSock, IocpBuff, proto);
	PostAcceptClient(IocpListenSock);
}

#ifdef KCP_SUPPORT
static void Kcp_recved(IOCP_SOCKET* IocpSock, IOCP_BUFF* IocpBuff)
{
	Kcp_Content* ctx = (Kcp_Content*)(IocpSock->_user_data);
	ikcp_input(ctx->kcp, IocpSock->recv_buf, IocpBuff->offset);
	HsocketSkipBuf(IocpSock, IocpBuff->offset);

	BaseProtocol* proto = NULL;
	int n = 0;
	int left = 0;
	while (1) {
		left = ctx->size - ctx->offset;
		if (left < ikcp_peeksize(ctx->kcp))
		{
			char* newbuf = (char*)realloc(ctx->buf, ctx->size + DATA_BUFSIZE);
			if (!newbuf) return;
			ctx->buf = newbuf;
			left = ctx->size - ctx->offset;
		}
		n = ikcp_recv(ctx->kcp, ctx->buf, left);
		if (n < 0) break;
		ctx->offset += n;

		proto = IocpSock->_user;
		if (IocpSock->fd != INVALID_SOCKET)
		{
			proto->Lock();
			if (IocpSock->fd != INVALID_SOCKET)
			{
				proto->ConnectionRecved(IocpSock, ctx->buf, ctx->offset);
				proto->UnLock();
			}
			else
			{
				proto->UnLock();
				return Close(IocpSock, IocpBuff, 0);
			}
		}
		else
		{
			return Close(IocpSock, IocpBuff, 0);
		}
	}
	PostRecv(IocpSock, IocpBuff, proto);
}
#endif

static void ProcessIO(IOCP_SOCKET* IocpSock, IOCP_BUFF* IocpBuff)
{
	BaseProtocol* proto = NULL;
	switch (IocpBuff->type)
	{
	case READ:
#ifdef KCP_SUPPORT
		if (IocpSock->_conn_type == KCP_CONN)
		{
			return Kcp_recved(IocpSock, IocpBuff);
		}
#endif
		proto = IocpSock->_user;
		if (IocpSock->fd != INVALID_SOCKET)
		{
			proto->Lock();
			if (IocpSock->fd != INVALID_SOCKET)
			{
				proto->ConnectionRecved(IocpSock, IocpSock->recv_buf, IocpBuff->offset);
				proto->UnLock();
			}
			else
			{
				proto->UnLock();
				return Close(IocpSock, IocpBuff, 0);
			}

			PostRecv(IocpSock, IocpBuff, proto);
		}
		else
		{
			return Close(IocpSock, IocpBuff, 0);
		}
		break;
	case WRITE:
		return Close(IocpSock, IocpBuff, 0);
		break;
	case ACCEPT:
		AceeptClient(IocpSock, IocpBuff);
		break;
	case CONNECT:
		proto = IocpSock->_user;
		if (IocpSock->fd != INVALID_SOCKET)
		{
			proto->Lock();
			if (IocpSock->fd != INVALID_SOCKET)
			{
				proto->ConnectionMade(IocpSock);
				proto->UnLock();
			}
			else
			{
				proto->UnLock();
				return Close(IocpSock, IocpBuff, 0);
			}

			PostRecv(IocpSock, IocpBuff, proto);
		}
		else
		{
			return Close(IocpSock, IocpBuff, 0);
		}
		break;
	default:
		break;
	}
}

/////////////////////////////////////////////////////////////////////////
//服务线程
DWORD WINAPI serverWorkerThread(LPVOID pParam)
{
	Reactor* reactor = (Reactor*)pParam;

	DWORD	dwIoSize = 0;
	IOCP_SOCKET* IocpSock = NULL;
	IOCP_BUFF* IocpBuff = NULL;		//IO数据,用于发起接收重叠操作
	bool bRet = false;
	DWORD err = 0;
	while (true)
	{
		bRet = false;
		dwIoSize = 0;	//IO操作长度
		IocpSock = NULL;
		IocpBuff = NULL;
		err = 0;
		bRet = GetQueuedCompletionStatus(reactor->ComPort, &dwIoSize, (PULONG_PTR)&IocpSock, (LPOVERLAPPED*)&IocpBuff, INFINITE);
		if (IocpBuff != NULL)
			IocpSock = IocpBuff->hsock;   //强制closesocket后可能返回错误的IocpSock，从IocpBuff中获取正确的IocpSock
		if (bRet == false)
		{
			err = GetLastError();  //64L,121L,995L
			if (IocpBuff == NULL || WAIT_TIMEOUT == err || ERROR_IO_PENDING == err)
				continue;
			Close(IocpSock, IocpBuff, err);
			continue;
		}
		else if (0 == dwIoSize && (READ == IocpBuff->type || WRITE == IocpBuff->type))
		{
			Close(IocpSock, IocpBuff, err);
			continue;
		}
		else
		{
			IocpBuff->offset += dwIoSize;
			ProcessIO(IocpSock, IocpBuff);
		}
	}
	return 0;
}

DWORD WINAPI mainIOCPServer(LPVOID pParam)
{
	Reactor* reactor = (Reactor*)pParam;
	for (int i = 0; i < reactor->CPU_COUNT*2; i++)
	//for (unsigned int i = 0; i < 1; i++)
	{
		HANDLE ThreadHandle;
		ThreadHandle = CreateThread(NULL, 0, serverWorkerThread, pParam, 0, NULL);
		if (NULL == ThreadHandle) {
			return -4;
		}
		CloseHandle(ThreadHandle);
	}
	std::map<uint16_t, BaseFactory*>::iterator iter;
	while (reactor->Run)
	{
		for (iter = reactor->FactoryAll.begin(); iter != reactor->FactoryAll.end(); ++iter)
		{
			iter->second->FactoryLoop();
		}
		Sleep(20);
	}
	return 0;
}

int __STDCALL ReactorStart(Reactor* reactor)
{
	WSADATA wsData;
	if (0 != WSAStartup(0x0202, &wsData))
	{
		return SOCKET_ERROR;
	}

	reactor->ComPort = CreateIoCompletionPort(INVALID_HANDLE_VALUE, NULL, 0, 0);
	if (reactor->ComPort == NULL)
	{
		return -2;
	}

	SYSTEM_INFO sysInfor;
	GetSystemInfo(&sysInfor);
	reactor->CPU_COUNT = sysInfor.dwNumberOfProcessors;

	SOCKET tempSock = WSASocket(AF_INET, SOCK_STREAM, 0, NULL, 0, WSA_FLAG_OVERLAPPED);
	//使用WSAIoctl获取AcceptEx函数指针
	DWORD dwbytes = 0;
	GUID guidAcceptEx = WSAID_ACCEPTEX;
	if (0 != WSAIoctl(tempSock, SIO_GET_EXTENSION_FUNCTION_POINTER,
		&guidAcceptEx, sizeof(guidAcceptEx),
		&reactor->lpfnAcceptEx, sizeof(reactor->lpfnAcceptEx),
		&dwbytes, NULL, NULL))
	{
		return -3;
	}
	closesocket(tempSock);

	HANDLE ThreadHandle;
	ThreadHandle = CreateThread(NULL, 0, mainIOCPServer, reactor, 0, NULL);
	if (NULL == ThreadHandle) {
		return -4;
	}
	CloseHandle(ThreadHandle);
	return 0;
}

void __STDCALL ReactorStop(Reactor* reactor)
{
	reactor->Run = false;
}

int __STDCALL FactoryRun(BaseFactory* fc)
{	
	if (!fc->FactoryInit())
		return -1;

	if (fc->ServerPort != 0)
	{
		fc->Listenfd = GetListenSock(fc->ServerAddr, fc->ServerPort);
		if (fc->Listenfd == SOCKET_ERROR)
			return -2;

		IOCP_SOCKET* IcpSock = NewIOCP_Socket();
		if (IcpSock == NULL)
		{
			closesocket(fc->Listenfd);
			return -3;
		}
		IcpSock->factory = fc;
		IcpSock->fd = fc->Listenfd;

		CreateIoCompletionPort((HANDLE)fc->Listenfd, fc->reactor->ComPort, (ULONG_PTR)IcpSock, 0);
		for (int i = 0; i < fc->reactor->CPU_COUNT; i++)
			PostAcceptClient(IcpSock);
	}
	
	fc->reactor->FactoryAll.insert(std::pair<uint16_t, BaseFactory*>(fc->ServerPort, fc));
	return 0;
}

int __STDCALL FactoryStop(BaseFactory* fc)
{
	std::map<uint16_t, BaseFactory*>::iterator iter;
	iter = fc->reactor->FactoryAll.find(fc->ServerPort);
	if (iter != fc->reactor->FactoryAll.end())
	{
		fc->reactor->FactoryAll.erase(iter);
	}
	fc->FactoryClose();
	return 0;
}

static bool IOCPConnectUDP(BaseFactory* fc, IOCP_SOCKET* IocpSock, IOCP_BUFF* IocpBuff)
{
	IocpSock->fd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (IocpSock->fd == INVALID_SOCKET)
		return false;
	IocpBuff->fd = IocpSock->fd;

	sockaddr_in local_addr;
	memset(&local_addr, 0, sizeof(sockaddr_in));
	local_addr.sin_family = AF_INET;
	bind(IocpSock->fd, (sockaddr*)(&local_addr), sizeof(sockaddr_in));

	if (ResetIocp_Buff(IocpSock, IocpBuff) == false)
	{
		closesocket(IocpSock->fd);
		return false;
	}

	CreateIoCompletionPort((HANDLE)IocpSock->fd, fc->reactor->ComPort, (ULONG_PTR)IocpSock, 0);

	int fromlen = sizeof(struct sockaddr);
	IocpBuff->type = READ;

	if (SOCKET_ERROR == WSARecvFrom(IocpSock->fd, &IocpBuff->databuf, 1, NULL, &(IocpBuff->flags), (sockaddr*)&IocpSock->peer_addr, &fromlen, &IocpBuff->overlapped, NULL))
	{
		if (ERROR_IO_PENDING != WSAGetLastError())
		{
			closesocket(IocpSock->fd);
			return false;
		}
	}
	return true;
}

static bool IOCPConnectTCP(BaseFactory* fc, IOCP_SOCKET* IocpSock, IOCP_BUFF* IocpBuff)
{
	IocpSock->fd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (IocpSock->fd == INVALID_SOCKET)
		return false;
	IocpBuff->fd = IocpSock->fd;

	setsockopt(IocpSock->fd, SOL_SOCKET, SO_UPDATE_ACCEPT_CONTEXT, (char*)&(fc->Listenfd), sizeof(fc->Listenfd));

	sockaddr_in local_addr;
	memset(&local_addr, 0, sizeof(sockaddr_in));
	local_addr.sin_family = AF_INET;
	bind(IocpSock->fd, (sockaddr*)(&local_addr), sizeof(sockaddr_in));

	LPFN_CONNECTEX lpfnConnectEx = NULL;
	GUID GuidConnectEx = WSAID_CONNECTEX;
	DWORD dwBytes = 0;
	if (SOCKET_ERROR == WSAIoctl(IocpSock->fd, SIO_GET_EXTENSION_FUNCTION_POINTER,
		&GuidConnectEx, sizeof(GuidConnectEx),
		&lpfnConnectEx, sizeof(lpfnConnectEx), &dwBytes, 0, 0))
	{
		if (ERROR_IO_PENDING != WSAGetLastError())
		{
			closesocket(IocpSock->fd);
			return false;
		}
	}
	CreateIoCompletionPort((HANDLE)IocpSock->fd, fc->reactor->ComPort, (ULONG_PTR)IocpSock, 0);

	PVOID lpSendBuffer = NULL;
	DWORD dwSendDataLength = 0;
	DWORD dwBytesSent = 0;
	BOOL bResult = lpfnConnectEx(IocpSock->fd,
		(SOCKADDR*)&IocpSock->peer_addr,	// [in] 对方地址
		sizeof(IocpSock->peer_addr),		// [in] 对方地址长度
		lpSendBuffer,			// [in] 连接后要发送的内容，这里不用
		dwSendDataLength,		// [in] 发送内容的字节数 ，这里不用
		&dwBytesSent,			// [out] 发送了多少个字节，这里不用
		&(IocpBuff->overlapped));

	if (!bResult)
	{
		if (WSAGetLastError() != ERROR_IO_PENDING)
		{
			closesocket(IocpSock->fd);
			return false;
		}
	}
	return true;
}

HSOCKET __STDCALL HsocketConnect(BaseProtocol* proto, const char* ip, int port, CONN_TYPE conntype)
{
	if (proto == NULL || (proto->sockCount == 0 && proto->protoType == SERVER_PROTOCOL))
		return NULL;
	BaseFactory* fc = proto->factory;
	IOCP_SOCKET* IocpSock = NewIOCP_Socket();
	if (IocpSock == NULL)
	{
		return NULL;
	}
	IOCP_BUFF* IocpBuff = NewIOCP_Buff();
	if (IocpBuff == NULL)
	{
		ReleaseIOCP_Socket(IocpSock);
		return NULL;
	}
	IocpBuff->type = CONNECT;
	IocpBuff->hsock = IocpSock;

	IocpSock->factory = fc;
	IocpSock->_conn_type = conntype > UDP_CONN ? TCP_CONN: conntype;
	IocpSock->_user = proto;
	IocpSock->_IocpBuff = IocpBuff;
	IocpSock->peer_addr.sin_family = AF_INET;
	IocpSock->peer_addr.sin_port = htons(port);
	inet_pton(AF_INET, ip, &IocpSock->peer_addr.sin_addr);

	bool ret = false;
	if (conntype == UDP_CONN)
		ret = IOCPConnectUDP(fc, IocpSock, IocpBuff);   //UDP连接
	else
		ret = IOCPConnectTCP(fc, IocpSock, IocpBuff);   //TCP连接

	if (ret == false)
	{
		ReleaseIOCP_Buff(IocpBuff);
		ReleaseIOCP_Socket(IocpSock);
		return NULL;
	}
	InterlockedIncrement(&proto->sockCount);
	return IocpSock;
}

static bool IOCPPostSendUDPEx(IOCP_SOCKET* IocpSock, IOCP_BUFF* IocpBuff)
{
	if (SOCKET_ERROR == WSASendTo(IocpSock->fd, &IocpBuff->databuf, 1, NULL, 0, (sockaddr*)&IocpSock->peer_addr, sizeof(IocpSock->peer_addr), &IocpBuff->overlapped, NULL))
	{
		if (ERROR_IO_PENDING != WSAGetLastError())
			return false;
	}
	return true;
}

static bool IOCPPostSendTCPEx(IOCP_SOCKET* IocpSock, IOCP_BUFF* IocpBuff)
{
	if (SOCKET_ERROR == WSASend(IocpSock->fd, &IocpBuff->databuf, 1, NULL, 0, &IocpBuff->overlapped, NULL))
	{
		if (ERROR_IO_PENDING != WSAGetLastError())
			return false;
	}
	return true;
}

bool __STDCALL HsocketSendEx(IOCP_SOCKET* IocpSock, const char* data, int len)
{
	if (IocpSock == NULL)
		return false;
	
	IOCP_BUFF* IocpBuff = NewIOCP_Buff();
	if (IocpBuff == NULL)
		return false;

	IocpBuff->databuf.buf = (char*)pst_malloc(len);
	if (IocpBuff->databuf.buf == NULL)
	{
		ReleaseIOCP_Buff(IocpBuff);
		return false;
	}
	memcpy(IocpBuff->databuf.buf, data, len);
	IocpBuff->databuf.len = len;
	memset(&IocpBuff->overlapped, 0, sizeof(OVERLAPPED));
	IocpBuff->type = WRITE;

	bool ret = false;
	if (IocpSock->_conn_type == TCP_CONN)
		ret = IOCPPostSendTCPEx(IocpSock, IocpBuff);
	else
		ret = IOCPPostSendUDPEx(IocpSock, IocpBuff);
	
	if (ret == false)
	{
		pst_free(IocpBuff->databuf.buf);
		ReleaseIOCP_Buff(IocpBuff);
		return false;
	}
	return true;
}

bool __STDCALL HsocketSend(IOCP_SOCKET* IocpSock, const char* data, int len)
{
#ifdef KCP_SUPPORT
	if (IocpSock->_conn_type == KCP_CONN)
	{
		Kcp_Content* ctx = (Kcp_Content*)IocpSock->_user_data;
		ikcp_send(ctx->kcp, data, len);
		return true;
	}
#endif
	return HsocketSendEx(IocpSock, data, len);
}

IOCP_BUFF* __STDCALL HsocketGetBuff()
{
	IOCP_BUFF* IocpBuff = NewIOCP_Buff();
	if (IocpBuff)
	{
		IocpBuff->databuf.buf = (char*)pst_malloc(DATA_BUFSIZE);
		if (IocpBuff->databuf.buf)
			IocpBuff->size = DATA_BUFSIZE;
	}
	return IocpBuff;
}

bool __STDCALL HsocketSetBuff(IOCP_BUFF* IocpBuff, const char* data, int len)
{
	if (IocpBuff == NULL) return false;
	int left = IocpBuff->size - IocpBuff->offset;
	if (left >= len)
	{
		memcpy(IocpBuff->databuf.buf + IocpBuff->databuf.len, data, len);
		IocpBuff->databuf.len += len;
	}
	else
	{
		char* new_ptr = (char*)pst_realloc(IocpBuff->databuf.buf, (size_t)IocpBuff->size + len);
		if (new_ptr)
		{
			IocpBuff->databuf.buf = new_ptr;
			memcpy(IocpBuff->databuf.buf + IocpBuff->databuf.len, data, len);
			IocpBuff->databuf.len += len;
		}
	}
	return true;
}

bool __STDCALL HsocketSendBuff(IOCP_SOCKET* IocpSock, IOCP_BUFF* IocpBuff)
{
	if (IocpBuff == NULL || IocpSock == NULL) return false;
	memset(&IocpBuff->overlapped, 0, sizeof(OVERLAPPED));
	IocpBuff->type = WRITE;

	bool ret = false;
	if (IocpSock->_conn_type == UDP_CONN)
		ret = IOCPPostSendUDPEx(IocpSock, IocpBuff);
	else
		ret = IOCPPostSendTCPEx(IocpSock, IocpBuff);

	if (ret == false)
	{
		pst_free(IocpBuff->databuf.buf);
		ReleaseIOCP_Buff(IocpBuff);
		return false;
	}
	return true;
}

bool __STDCALL HsocketClose(IOCP_SOCKET* IocpSock)
{
	if (IocpSock == NULL ||IocpSock->fd == INVALID_SOCKET || IocpSock->fd == NULL)
		return false;
	SOCKET fd = InterlockedExchange(&IocpSock->fd, NULL);
	if (fd != INVALID_SOCKET && fd != NULL)
	{
		closesocket(fd);
	}
	return true;
}

int __STDCALL HsocketSkipBuf(IOCP_SOCKET* IocpSock, int len)
{
	IocpSock->_IocpBuff->offset -= len;
	memmove(IocpSock->recv_buf, IocpSock->recv_buf + len, IocpSock->_IocpBuff->offset);
	return IocpSock->_IocpBuff->offset;
}

void __STDCALL HsocketPeerIP(HSOCKET hsock, char* ip, size_t ipsz)
{
	inet_ntop(AF_INET, &hsock->peer_addr.sin_addr, ip, ipsz);
}
int __STDCALL HsocketPeerPort(HSOCKET hsock) 
{
	return ntohs(hsock->peer_addr.sin_port);
}

#ifdef KCP_SUPPORT
static int kcp_send_callback(const char* buf, int len, ikcpcb* kcp, void* user)
{
	HsocketSendEx((HSOCKET)user, buf, len);
	return 0;
}

int __STDCALL HsocketKcpCreate(HSOCKET hsock, int conv)
{
	ikcpcb* kcp = ikcp_create(conv, hsock);
	if (!kcp) return -1;
	kcp->output = kcp_send_callback;
	Kcp_Content* ctx = (Kcp_Content*)malloc(sizeof(Kcp_Content));
	if (!ctx) { ikcp_release(kcp); return -1; }
	ctx->kcp = kcp;
	ctx->buf = (char*)malloc(sizeof(DATA_BUFSIZE));
	if (!ctx->buf) { ikcp_release(kcp); free(ctx); return -1; }
	ctx->size = DATA_BUFSIZE;
	hsock->_user_data = ctx;
	hsock->_conn_type = KCP_CONN;
	return 0;
}

int __STDCALL HsocketKcpNodelay(HSOCKET hsock, int nodelay, int interval, int resend, int nc)
{
	Kcp_Content* ctx = (Kcp_Content*)hsock->_user_data;
	ikcp_nodelay(ctx->kcp, nodelay, interval, resend, nc);
	return 0;
}

int __STDCALL HsocketKcpWndsize(HSOCKET hsock, int sndwnd, int rcvwnd)
{
	Kcp_Content* ctx = (Kcp_Content*)hsock->_user_data;
	ikcp_wndsize(ctx->kcp, sndwnd, rcvwnd);
	return 0;
}

int __STDCALL HsocketKcpGetconv(HSOCKET hsock)
{
	Kcp_Content* ctx = (Kcp_Content*)hsock->_user_data;
	return ikcp_getconv(ctx->kcp);
}

void __STDCALL HsocketKcpEnable(HSOCKET hsock, char enable)
{
	Kcp_Content* ctx = (Kcp_Content*)hsock->_user_data;
	ctx->enable = enable;
}
#endif
